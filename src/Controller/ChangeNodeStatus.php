<?php

namespace Drupal\enable_disable_node_toggle\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;

/**
 * Change node status.
 */
class ChangeNodeStatus extends ControllerBase {

  /**
   * Process the API response.
   *
   * @return array
   *   Node status.
   */
  public function enableDisableStatus(ServerRequestInterface $request) {
    $body = $request->getParsedBody();
    $decoded = Json::decode($body['title']);
    $node = Node::load($decoded['nid']);
    if ($decoded['status'] == 'active') {
      $node->setPublished(TRUE);
    }
    else {
      $node->setUnpublished();
    }
    $node->save();
    return new JsonResponse($decoded);
  }

}
