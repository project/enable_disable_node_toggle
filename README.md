INTRODUCTION
------------

This module provides Enable/Disable node status on Views pages.


REQUIREMENTS
-------------

Views, Views UI, Filter, Node, System module enabled.


INSTALLATION
------------

Enable this module from extend list page /admin/modules.


CONFIGURATION
-------------

1) Activate Enable/Disable node toggle module at /admin/modules.
2) Create a new view
    - Goto '/admin/structure/views/add' on your site.
    - Check off 'Create a page'.
    - Check off 'Create a block'.
    - Set the 'Display format' for the page to what you desire.
    - Set the "'Display format' of" to fields.
    - Set the 'Display format' for the block to table.
    - Fill in the rest of the views information.
    - Click Save & edit button.
3) Under the "FIELDS" section, do you see "Content: Title"?  If you do not:
    - Click 'add' button at the "Fields" section and choose field
    "Content:title", add and apply.
4) Add the "Enable/Disable Node Toggle" Field:
    - Click Add button at the "FIELDS" section.
5) Save the view and you're done.
