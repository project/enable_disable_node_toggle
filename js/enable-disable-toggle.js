/**
 * @file
 * Attaches behaviour for htp module.
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.enable_disable_node_toggle = {
    attach: function attach(context, settings) {
        $('.change-status-toggle').each(function () {
         if($(this).parent('.switch').length == 0) {
           $(this).wrap('<label class="switch"></label>');
           var checked_status = $(this).data('checkstatus');
           var nid = $(this).data('nid');
           var row_no = $(this).data('row');
           $('<input type="checkbox" name="change-status[' + row_no + ']" data-node=' + nid + ' ' + checked_status + '>').insertBefore(this);
           $(this).removeClass('d-none');
          }
        });

        $('.change-status-toggle').on('click', function () {
           var row_no = $(this).data('row');
           var status = (!$('input[name="change-status[' + row_no + ']"]').is(':checked')) ? "active" : "inactive";
           var jsonObjects = {nid:$(this).data('nid'), status: status};
           $.ajax({
              url: settings.path.baseUrl + 'change-node-status',
              type: "POST",
              data: {title: JSON.stringify(jsonObjects) },
              dataType: "json",
              beforeSend: function (x) {
              $('.ajax-progress-throbber.current-' + row_no).show();
              $('.ajax-progress__throbber.current-' + row_no).show();
              if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
              }
              },
              success : function () {
                $('.ajax-progress-throbber.current-' + row_no).hide();
                $('.ajax-progress__throbber.current-' + row_no).hide();
              }
            });
        });
    }
  };
}(jQuery, Drupal));
