<?php

namespace Drupal\enable_disable_node_toggle\Plugin\views\field;

use Drupal\Core\Render\Markup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Defines a enable/disable form element.
 *
 * @ViewsField("enable_disable_views_field")
 */
class EnableDisableNode extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['enable_disable_help'] = [
      '#markup' => $this->t("Markup will be added to each row of the table."),
    ];
    parent::buildOptionsForm($form, $form_state);

    $form['include_exclude']['#access'] = FALSE;
    $form['selected_actions']['#access'] = FALSE;
    $form['exclude']['#access'] = FALSE;
    $form['alter']['#access'] = FALSE;
    $form['empty_field_behavior']['#access'] = FALSE;
    $form['empty']['#access'] = FALSE;
    $form['empty_zero']['#access'] = FALSE;
    $form['hide_empty']['#access'] = FALSE;
    $form['hide_alter_empty']['#access'] = FALSE;

  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($values->_entity->status->getValue()[0]['value'] == 0) {
      $markup = '<span class="slider round change-status-toggle d-none" data-checkstatus="" data-nid="' . $values->_entity->id() . '" data-row="' . $this->view->row_index . '"></span>
<div class="enable-disable ajax-progress ajax-progress-throbber ajax-progress__throbber current-' . $this->view->row_index . '"><div class="throbber">&nbsp;</div></div>
';
    }
    else {
      $markup = '<span class="slider round change-status-toggle d-none" data-checkstatus="checked" data-nid="' . $values->_entity->id() . '" data-row="' . $this->view->row_index . '"></span>
<div class="enable-disable ajax-progress ajax-progress-throbber ajax-progress__throbber current-' . $this->view->row_index . '"><div class="throbber">&nbsp;</div></div>
';
    }

    return Markup::create($markup);
  }

}
